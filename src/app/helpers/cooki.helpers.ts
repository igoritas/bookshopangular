import { Injectable } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';

@Injectable()
export class CookiHelper {

    constructor(private cookieService: CookieService) 
    { }

    setCookieForExpire(nameParam: string[], valueParam: string[], timeExpire: Date | number): void {
        for (let i = 0; i < nameParam.length; i++) {
            this.cookieService.delete(nameParam[i]);
            this.cookieService.set(nameParam[i], valueParam[i], timeExpire);
        }
    }

    checkСookie(nameParam: string, checkedValue: string): boolean {
        return this.cookieService.get(nameParam) == checkedValue;
    }

    deleteAllCookie(): void {
        this.cookieService.deleteAll();
    }

    getAllCookie(): {} {
        return this.cookieService.getAll();
    }

    get(item: string): string {
        return this.cookieService.get(item);
    }

}