import { Pipe, PipeTransform } from '@angular/core';
import { GlobalConstant } from 'src/app/sheared/global.constant';

@Pipe({
  name: 'eNumAsString'
})

export class ENumAsStringPipe implements PipeTransform {
    printingEditionType: string[];
      constructor(private globalConstant: GlobalConstant){
        this.printingEditionType = globalConstant.printingEditionString;
      }
    transform(value: number): string {
        return this.printingEditionType[value];
    }
}