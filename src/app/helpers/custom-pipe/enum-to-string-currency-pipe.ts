import { Pipe, PipeTransform } from '@angular/core';
import { GlobalConstant } from 'src/app/sheared/global.constant';

@Pipe({
  name: 'eNumAsStringCurrency'
})

export class ENumAsStringCurrency implements PipeTransform {
    curencyType;
      constructor(globalConstant: GlobalConstant){
        this.curencyType = globalConstant.currencyFilter;
      }
    transform(value: number): string {
        return this.curencyType[value].currency;
    }

}