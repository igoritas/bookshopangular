import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NotificationService } from 'src/app/services/notification.service';
import { UserService } from 'src/app/services/user.service';
import { GlobalConstant } from 'src/app/sheared/global.constant';
import { StorageHelper } from 'src/app/helpers/storage.helpers';
@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  firstName: string;
  lastName: string;
  email: string;
  isAdmin: boolean;
  EditForm: FormGroup;
  submitted: boolean;


  constructor(
    private userService: UserService,
    private notificatio: NotificationService,
    private navigate: Router,
    private globalConstant: GlobalConstant,
    private storageHelper: StorageHelper
  ) {
    this.EditForm = new FormGroup({
      'firstName': new FormControl(globalConstant.stringEmpty, Validators.required),
      'lastName': new FormControl(globalConstant.stringEmpty, Validators.required),
      'email': new FormControl(globalConstant.stringEmpty, [Validators.required, Validators.email]),
      'oldPassword': new FormControl(globalConstant.stringEmpty, [Validators.min(6)]),
      'newPassword': new FormControl(globalConstant.stringEmpty, [Validators.min(6)])
    });
    this.submitted = false;
  }
  get f() { return this.EditForm.controls };

  ngOnInit() {
    if (this.storageHelper.getByKey(this.globalConstant.firstNameOfStorage) == null && this.storageHelper.getByKey(this.globalConstant.lastNameOfStorage) == null) {
      this.navigate.navigate(['/login']);
      return;
    }
    this.firstName = this.storageHelper.getByKey(this.globalConstant.firstNameOfStorage);
    this.lastName = this.storageHelper.getByKey(this.globalConstant.lastNameOfStorage);
    this.email = this.storageHelper.getByKey(this.globalConstant.emailOfStorage);
    this.isAdmin = this.storageHelper.getByKey(this.globalConstant.roleOfStorage).toLowerCase() == 'true';
  }
  editProfile(): void {
    this.submitted = true;
    if (this.EditForm.invalid) {
      return;
    }
    this.userService.updateUser(this.EditForm.value).subscribe((date: any) => {
      if (date.errors.length != 0) {
        this.notificatio.showError(date.errors);
        return;
      }
      this.storageHelper.setItem(this.globalConstant.firstNameOfStorage, this.firstName);
        this.storageHelper.setItem(this.globalConstant.lastNameOfStorage, this.lastName);
        this.storageHelper.setItem(this.globalConstant.emailOfStorage, this.email);
        location.reload();
    })
  }
}
