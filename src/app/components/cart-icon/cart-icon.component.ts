import { Component, OnInit } from '@angular/core';
import { CartService } from 'src/app/services/cart.service';

@Component({
  selector: 'app-cart-icon',
  templateUrl: './cart-icon.component.html',
  styleUrls: ['./cart-icon.component.scss']
})
export class CartIconComponent implements OnInit {
  counter: number;
  constructor(private cartService: CartService) {
    this.cartService.count.subscribe(value => {
      this.counter = value;
    });
  }

  ngOnInit() {
    this.counter = this.cartService.orderItemsList.length;
  }

}
