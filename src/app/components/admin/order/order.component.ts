import { Component, OnInit, ViewChild } from '@angular/core';
import { OrderService } from 'src/app/services/order-service.service';
import { GlobalConstant } from 'src/app/sheared/global.constant';
import { MatDialog, MatPaginator, MatSort, PageEvent, TableConstant } from '../index';
import {SortName,SortType} from '../../../models/enums/index';
import {FilterModel,Toggle} from '../../../models/filter/index';
import {Order} from '../../../models/order/index';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.scss']
})
export class OrderComponent implements OnInit {
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;

  private readonly defaultCountChacked: number = 0;
  orderList: Order[];
  filterModel: FilterModel;

  public displayedColumns: string[];
  public dataSource;

  pageEvent: PageEvent;
  pageIndex: number;
  pageSize: number;
  length: number;

  resizeOptionTable: number[];

  toggleList: Toggle[];

  constructor(
    private orderService: OrderService,
    public dialog: MatDialog,
    public globalConstant: GlobalConstant,
    public tableConstant: TableConstant
  ) {
    this.toggleList = globalConstant.togglOreder;
    this.orderList = [];
    this.displayedColumns = tableConstant.displayedColumnOrder;
    this.resizeOptionTable = tableConstant.defaultResizeOptionValue;
  }

  ngOnInit() {
    this.filterModel = {
      allCount: this.globalConstant.numberEmpty,
      searchString: this.globalConstant.stringEmpty,
      paginationModel: {
        pageNumber: this.tableConstant.defaultPageIndexValue,
        pageSize: this.tableConstant.defaultPageSizeValue,
      },
      items: [],
      filterItems: [],
      sortName: SortName.Id,
      sortType: SortType.asc
    }
    this.getFirstPage()
  }
  private getFirstPage(): void {
    let firstPage = new PageEvent();
    firstPage.pageIndex = this.tableConstant.defaultPageIndexValue;
    firstPage.pageSize = this.filterModel.paginationModel.pageSize;
    this.getFiltredData(firstPage);
  }
  private sortOptions(event: MatSort): void {
    this.filterModel.sortType = event.direction == SortType[SortType.asc] ? SortType.asc : SortType.desc;
    this.getFirstPage();

  }

  sortData(event: MatSort): void {
    if (event.direction != this.globalConstant.stringEmpty && event.active == SortName[SortName.Id]) {
      this.filterModel.sortName = SortName.Id;
      this.sortOptions(event);
    }
    if (event.direction != this.globalConstant.stringEmpty && event.active == SortName[SortName.CreationDate]) {
      this.filterModel.sortName = SortName.CreationDate;
      this.sortOptions(event);
    }
    if (event.direction != this.globalConstant.stringEmpty && event.active == SortName[SortName.OrderPrice]) {
      this.filterModel.sortName = SortName.OrderPrice;
      this.sortOptions(event);
    }
  }
  getFiltredData(event?: PageEvent): PageEvent {
    this.filterOrder();
    this.filterModel.paginationModel.pageNumber = event.pageIndex;
    this.filterModel.paginationModel.pageSize = event.pageSize;
    this.setFilteredData(this.filterModel);
    return event;
  }
  private setFilteredData(filterModel:FilterModel):void{
    this.orderService.getFiltredOrderData(filterModel).subscribe(
      (response: FilterModel) => {
        if (response.items == null) {
          return;
        }
          this.dataSource = response.items;
          this.length = response.allCount;
      }
    );
    this.filterModel.filterItems = [];
  }
  filterOrder(): void {
    for (let index = 0; index < this.toggleList.length; index++) {
      if (this.toggleList[index].isCheck) {
        continue;
      }
      this.filterModel.filterItems.push({
        columnName: this.toggleList[index].name,
        value: this.toggleList[index].value
      });
    }
  }
  
  checkValue(event: any, index: number): void {
    let countChackdBox = this.defaultCountChacked;
    for (let index = 0; index < this.toggleList.length; index++) {
      if (!this.toggleList[index].isCheck) {
        continue;
      }
      countChackdBox++;
    }
    if (countChackdBox != this.defaultCountChacked) {
      return;
    }
    this.toggleList[index].isCheck = true;
      event.source.toggle();
  }
}
