import { Component, OnInit, ViewChild } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { GlobalConstant } from 'src/app/sheared/global.constant';
import { DialogTemplateBoolResultComponent } from '../dialogs/dialog-template-bool-result/dialog-template-bool-result.component';
import { DialogUserComponent } from '../dialogs/dialog-user/dialog-user.component';
import { DialogConstant, MatDialog, MatPaginator, MatSort, PageEvent, TableConstant } from '../index';
import {SortName,SortType} from '../../../models/enums/index';
import {FilterModel,Toggle} from '../../../models/filter/index';
import {User,EditUserModel} from '../../../models/user/index';
import { BaseModel } from 'src/app/models/base/base.Model';


@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {


  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;
  private readonly defaultCountChacked: number = 0;
  userList: User[];
  public displayedColumns: string[];

  toggleActiveUser: Toggle[];
  filterUserModel: FilterModel;

  public dataSource;
  pageEvent: PageEvent;
  pageIndex: number;
  pageSize: number;
  length: number;
  resizeOptionTable: number[];

  constructor(
    private userService: UserService,
    public dialog: MatDialog,
    public globalConstant: GlobalConstant,
    public tableConstant: TableConstant,
    public dialogConstant: DialogConstant

  ) {
    this.toggleActiveUser = globalConstant.toggleActive;
    this.displayedColumns = tableConstant.diplayedColumnUser;
    this.resizeOptionTable = tableConstant.defaultResizeOptionValue;
    this.userList = [];
  }
  ngOnInit(): void {
    this.filterUserModel = {
      allCount: this.globalConstant.numberEmpty,
      searchString: this.globalConstant.stringEmpty,
      paginationModel: {
        pageNumber: this.tableConstant.defaultPageIndexValue,
        pageSize: this.tableConstant.defaultPageSizeValue
      },
      items: [],
      filterItems: [],
      sortName: SortName.Id,
      sortType: SortType.asc
    };
    this.getFirstPage()
  }
  private getFirstPage(): void {
    let firstPage = new PageEvent();
    firstPage.pageIndex = this.tableConstant.defaultPageIndexValue;
    firstPage.pageSize = this.filterUserModel.paginationModel.pageSize;
    this.getFilteredData(firstPage);
  }
  searchFilter(filterValue: string): void {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    this.filterUserModel.searchString = this.dataSource.filter;
    this.getFirstPage()
  }
  filterUser(): void {
    for (let index = this.globalConstant.numberEmpty; index < this.toggleActiveUser.length; index++) {
      if (this.toggleActiveUser[index].isCheck) {
        continue;
      }
      this.filterUserModel.filterItems.push({
        columnName: this.toggleActiveUser[index].name,
        value: this.toggleActiveUser[index].value
      });
    }
  }
  getFilteredData(event?: PageEvent): PageEvent {
    this.filterUser();
    this.filterUserModel.paginationModel.pageNumber = event.pageIndex;
    this.filterUserModel.paginationModel.pageSize = event.pageSize;

    this.setFilteredData(this.filterUserModel);
    return event;
  }
  private setFilteredData(filterModel:FilterModel):void{
    this.userService.getFiltredUsersData(filterModel).subscribe(
      (response: FilterModel) => {
        if (response.items == null) {
          return;
        }
          this.dataSource = response.items;
          this.length = response.allCount;
      }
    );
    this.filterUserModel.filterItems = [];
  }
  delete(item: User): void {
    const dialogRef = this.dialog.open(DialogTemplateBoolResultComponent, {
      data: {
        titleOk: this.dialogConstant.defaultDeleteTitleOk,
        titleCancel: this.dialogConstant.defaultDeleteTitleCancel,
        titleWindow: `Delete ${item.userName}`,
        textContent: this.dialogConstant.defaultDeleteTextContent
      }
    });
    dialogRef.afterClosed().subscribe((result: boolean) => {
      if (!result) {
        return;
      }
      this.userService.deleteUser(item.id).subscribe(
        (response: BaseModel) => {
          if (response.errors.length != 0) {
            return;
          }
          location.reload();
        }
      );
    });

  }
  change(user: User): void {
    const dialogRef = this.dialog.open(DialogUserComponent, {
      width: this.dialogConstant.defaultDialogWidth,
      data: user
    });

    dialogRef.afterClosed().subscribe((result: EditUserModel) => {
      if (result == null) {
        return;
      }
      this.userService.updateUser(result).subscribe(
        (response: BaseModel) => {

          if (response.errors.length != 0) {
            return;
          }

          location.reload();

        }
      );
    });

  }

  checkValue(event: any, index: number): void {
    let countChackedBox = this.defaultCountChacked;
    for (let index = this.globalConstant.numberEmpty; index < this.toggleActiveUser.length; index++) {
      if (!this.toggleActiveUser[index].isCheck) {
        continue;
      }
      countChackedBox++;
    }

    if (countChackedBox != this.defaultCountChacked) {
      return;
    }

    this.toggleActiveUser[index].isCheck = true;
    event.source.toggle();
  }
}
