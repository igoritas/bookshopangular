import { Component, Inject, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { AuthorService } from 'src/app/services/author.service';
import { GlobalConstant } from 'src/app/sheared/index';
import {CurrencyFilter,Toggle} from '../../../../models/filter/index';
import { AuthorModel } from 'src/app/models/author/author.Model';
import { PrintingEdition } from 'src/app/models/printingEdition/printingEdition.Model';

@Component({
  selector: "dialog-overview-example-dialog",
  templateUrl: "/dialog-overview-example-dialog.html"
})
export class DialogOverviewExampleDialog implements OnInit {
  public authorsControl: FormControl;
  public currencyControl: FormControl;
  currencyFilterModels: CurrencyFilter[];
  authorModels: AuthorModel[];
  printingEditionType: Toggle[];
  ngOnInit(): void {
    this.authorService.getAuthors().subscribe(date => this.authorModels = date);
  }
  constructor(
    private dialogRef: MatDialogRef<DialogOverviewExampleDialog>,
    private authorService: AuthorService,
    globalConstant: GlobalConstant,
    @Inject(MAT_DIALOG_DATA) public data: PrintingEdition
  ) {
    this.authorsControl = new FormControl();
    this.currencyControl = new FormControl();
    this.printingEditionType = globalConstant.toggleBook;
    this.saveAuthorList();
    this.currencyFilterModels = globalConstant.currencyFilter;

  }
  private saveAuthorList(): void {
    if (this.data.authors.length == 0) {
      return;
    }
    this.authorsControl.setValue(this.data.authors);
  }
  compareAuthor(author1: AuthorModel, author2: AuthorModel) {
    return (author1.firstName + author1.lastName) == (author2.firstName + author2.lastName);
  }
  compareCurrency(currency1: string, currency2: string) {
    return currency1 == currency2;
  }
  compareTypeBook(type1: Number, type2: Number) {
    return type1 == type2;
  }
  onNoClick(): void {
    debugger
    this.data.authors = this.authorsControl.value;
    this.data.currency = this.currencyControl.value;
    this.dialogRef.close(this.data);
  }
}
