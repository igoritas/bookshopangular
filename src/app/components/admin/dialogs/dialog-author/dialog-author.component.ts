import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { AuthorModel } from 'src/app/models/author/author.Model';


@Component({
  selector: 'app-dialog-athor',
  templateUrl: './dialog-author.component.html',
  styleUrls: ['./dialog-author.component.scss']
})
export class DialogAuthorComponent {

  constructor(private dialogRef: MatDialogRef<DialogAuthorComponent>,
    @Inject(MAT_DIALOG_DATA) public data: AuthorModel) { }

  onNoClick() {
    this.dialogRef.close(this.data);
  }

}
