import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';


@Component({
  selector: 'app-dialog-template-bool-result',
  templateUrl: './dialog-template-bool-result.component.html',
  styleUrls: ['./dialog-template-bool-result.component.scss']
})
export class DialogTemplateBoolResultComponent {

  constructor(
    private dialogRef: MatDialogRef<DialogTemplateBoolResultComponent>,
    @Inject(MAT_DIALOG_DATA) public data: {
      titleOk: string,
      titleCancel: string,
      titleWindow: string,
      textContent: string
    }
  ) { }

  onNoClick(response: boolean) : void {
    this.dialogRef.close(response);
  }
}
