import { DatePipe } from '@angular/common';
import { Component, OnInit, ViewChild } from "@angular/core";
import { PrintingEditionService } from "src/app/services/printingEdition.service";
import { OrderService } from 'src/app/services/order-service.service';
import { GlobalConstant } from 'src/app/sheared/global.constant';
import { DialogOverviewExampleDialog } from './dialogs/dialog-printing-edition/dialog-overview-example-dialog';
import { DialogTemplateBoolResultComponent } from './dialogs/dialog-template-bool-result/dialog-template-bool-result.component';
import { DialogConstant, MatDialog, MatPaginator, MatSort, MatTableDataSource, PageEvent, TableConstant } from './index';
import {Currency,PrinringEditionType,SortName,SortType} from '../../models/enums/index';
import {FilterModel,CurrencyFilter,SortPrintingEdition,Toggle} from '../../models/filter/index';
import { PrintingEdition } from 'src/app/models/printingEdition/printingEdition.Model';
import { BaseModel } from 'src/app/models/base/base.Model';


@Component({
  selector: "app-admin",
  templateUrl: "./admin.component.html",
  styleUrls: ["./admin.component.scss"],
})
export class AdminComponent implements OnInit {
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;

  private readonly defaultCountChacked: number = 0;

  bookList: PrintingEdition[];
  public displayedColumns: string[];
  currencyList:CurrencyFilter[];
  filterPrintingEditions: FilterModel;
  toggleList: Toggle[];
  sortList: SortPrintingEdition[];

  resizeOptionTable: number[];

  public dataSource;
  pageEvent: PageEvent;
  pageIndex: number;
  pageSize: number;
  length: number;

  constructor(
    private printingEditionService: PrintingEditionService,
    public dialog: MatDialog,
    public datepipe: DatePipe,
    public globalConstant: GlobalConstant,
    public tableConstant: TableConstant,
    private orderService: OrderService,
    public dialogConstant: DialogConstant
  ) {
    this.resizeOptionTable = tableConstant.defaultResizeOptionValue;
    this.bookList = [];
    this.displayedColumns = tableConstant.displayedColumnPrintingEdition;
    this.currencyList = globalConstant.currencyFilter;
  }

  private getFirstPage(): void {
    let firstPage = new PageEvent();
    firstPage.pageIndex = this.tableConstant.defaultPageIndexValue;
    firstPage.pageSize = this.filterPrintingEditions.paginationModel.pageSize;
    this.getFilteredData(firstPage);
  }

  private sortOptions(event: MatSort): void {
    this.filterPrintingEditions.sortType = event.direction == SortType[SortType.asc] ? SortType.asc : SortType.desc;
    this.getFirstPage()
  }

  sortData(event: MatSort): void {

    if (event.direction != this.globalConstant.stringEmpty && event.active == SortName[SortName.Id]) {
      this.filterPrintingEditions.sortName = SortName.Id;
      this.sortOptions(event);
    }

    if (event.direction != this.globalConstant.stringEmpty && event.active == SortName[SortName.Price]) {
      this.filterPrintingEditions.sortName = SortName.Price;
      this.sortOptions(event);
    }

  }

  searchFilter(filterValue: string): void {

    this.filterPrintingEditions.searchString = filterValue.trim().toLowerCase();

    this.getFirstPage();

  }

  ngOnInit() {

    this.toggleList = this.globalConstant.toggleBook;
    this.sortList = this.globalConstant.sortBookUserPresentetion;

    this.filterPrintingEditions = {
      searchString: this.globalConstant.stringEmpty,
      allCount: this.globalConstant.numberEmpty,
      paginationModel: {
        pageNumber: this.tableConstant.defaultPageIndexValue,
        pageSize: this.tableConstant.defaultPageSizeValue,
      },
      items: [],
      filterItems: [],
      sortName: SortName.Id,
      sortType: SortType.asc
    };

    this.getFirstPage();

  }

  changValueSummOfCarency(currency: number): void {
    this.bookList.forEach((element: PrintingEdition) => {
      if (element.currency.toString() == this.currencyList[currency].currency) {
        return;
       }
       this.setCoefficientCurrency(element, currency);
    });
  }

  private setCoefficientCurrency(element: PrintingEdition, currency: number) : void{
    this.orderService.ConvertCurrency(element.currency, this.currencyList[currency].value).subscribe((data:any) => {
      element.currency = this.currencyList[currency].value;
      element.price = element.price * data.result;
    }
    );
  }
  getFilteredData(event?: PageEvent): PageEvent {
    this.filterBook();

    this.filterPrintingEditions.paginationModel.pageNumber = event.pageIndex;
    this.filterPrintingEditions.paginationModel.pageSize = event.pageSize;

    this.setFiltredData(this.filterPrintingEditions);
   return event;
  }
  setFiltredData(filterModel: FilterModel){
    this.printingEditionService.getFiltredBookData(this.filterPrintingEditions).subscribe(
      (response: FilterModel) => {
        if (this.filterPrintingEditions.items == null) {
          return;
        }
        this.bookList = response.items;

        this.changValueSummOfCarency(Currency.USD)

        this.dataSource = new MatTableDataSource(this.bookList);
        this.length = response.allCount;
      }
    );
    this.filterPrintingEditions.filterItems = [];
  }
  filterBook(): void {
    for (let index = 0; index < this.toggleList.length; index++) {
      if (this.toggleList[index].isCheck) {
        continue;
      }
      this.filterPrintingEditions.filterItems.push({
        columnName: this.toggleList[index].name,
        value: this.toggleList[index].value
      });
    }
  }

  addBook(): void {
    let book: PrintingEdition = {
      id: this.globalConstant.stringEmpty,
      type: PrinringEditionType.Book,
      currency: Currency.USD,
      authors: [],
      discription: this.globalConstant.stringEmpty,
      name: this.globalConstant.stringEmpty,
      price: this.globalConstant.numberEmpty,
      publicationDate: new Date(),
      errors: []
    };
    const dialogRef = this.dialog.open(DialogOverviewExampleDialog, {
      width: this.dialogConstant.defaultDialogWidth,
      height: this.dialogConstant.defaultDialogHeight,
      data: book
    });

    dialogRef.afterClosed().subscribe((result: PrintingEdition) => {
      if (result == null) {
        return;
      }
      this.sendAddbook(result);
    });
  }
  private sendAddbook(book: PrintingEdition): void {
    this.printingEditionService.addBook(book).subscribe(
      (response: BaseModel) => {
        if (response.errors.length != 0) {
          return
        }
        location.reload();
      }
    );
  }


  change(item: PrintingEdition): void {
    const dialogRef = this.dialog.open(DialogOverviewExampleDialog, {
      width: this.dialogConstant.defaultDialogWidth,
      height: this.dialogConstant.defaultDialogHeight,
      data: item
    });

    dialogRef.afterClosed().subscribe((result: PrintingEdition) => {
      if (result == null) {
        return;
      }
      let bookChang: PrintingEdition = result;

      this.printingEditionService.update(bookChang).subscribe(
        (response: BaseModel) => {
          if (response.errors.length != 0) {
            return;
          }
          location.reload();
        }
      );
    });

  }

  delete(item: PrintingEdition): void {
    const dialogRef = this.dialog.open(DialogTemplateBoolResultComponent, {
      data: {
        titleOk: this.dialogConstant.defaultDeleteTitleOk,
        titleCancel: this.dialogConstant.defaultDeleteTitleCancel,
        titleWindow: `Delete ${item.name}`,
        textContent: this.dialogConstant.defaultDeleteTextContent
      }
    });

    dialogRef.afterClosed().subscribe((result: boolean) => {
      if (!result) {
        return;
      }
      this.printingEditionService.deletebook(item.id).subscribe(
        (response: BaseModel) => {
          if (response.errors.length != 0) {
            return;
          }
          location.reload();
        }
      );
    })
  }

  checkValue(event: any, index: number): void {
    let countChackedBox = this.defaultCountChacked;
    for (let index = this.globalConstant.numberEmpty; index < this.toggleList.length; index++) {
      if (!this.toggleList[index].isCheck) {
        continue;
      }
      countChackedBox++;
    }

    if (countChackedBox != this.defaultCountChacked) {
      return;
    }
    this.toggleList[index].isCheck = true;
      event.source.toggle();
  }
}
