import { Component, OnInit, ViewChild } from '@angular/core';
import { AuthorService } from 'src/app/services/author.service';
import { DialogAuthorComponent } from '../dialogs/dialog-author/dialog-author.component';
import { DatePipe } from '@angular/common';
import { DialogTemplateBoolResultComponent } from '../dialogs/dialog-template-bool-result/dialog-template-bool-result.component';
import { GlobalConstant } from '../../../sheared/index';
import { MatPaginator, MatSort, PageEvent, MatDialog, TableConstant, DialogConstant, MatTableDataSource } from '../index';
import {SortName,SortType} from '../../../models/enums/index';
import {FilterModel} from '../../../models/filter/index';
import { AuthorModel } from 'src/app/models/author/author.Model';
import { BaseModel } from 'src/app/models/base/base.Model';

@Component({
  selector: 'app-athor',
  templateUrl: './author.component.html',
  styleUrls: ['./author.component.scss']
})
export class AuthorComponent implements OnInit {

  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;

  authorList: AuthorModel[];
  resizeOptionTable: number[];
  filterAuthor: FilterModel;
  pageIndex: number;
  pageSize: number;
  length: number;
  public displayedColumns: string[];
  public dataSource;

  constructor(
    private authorService: AuthorService,
    public dialog: MatDialog,
    public datePipe: DatePipe,
    public globalConstant: GlobalConstant,
    public tableConstant: TableConstant,
    public dialogConstant: DialogConstant
  ) {

    this.displayedColumns = tableConstant.displayedColumnAuthor;
    this.resizeOptionTable = tableConstant.defaultResizeOptionValue;
    this.authorList = [];

  }

  private getFirstPage(): void {
    let firstPage = new PageEvent();

    firstPage.pageIndex = this.tableConstant.defaultPageIndexValue;
    firstPage.pageSize = this.filterAuthor.paginationModel.pageSize;

    this.getServerData(firstPage);
  }

  searchFilter(filterValue: string): void {
    this.filterAuthor.searchString = filterValue.trim().toLowerCase();

    this.getFirstPage();
  }

  ngOnInit(): void {
    this.filterAuthor = {
      allCount: this.globalConstant.numberEmpty,
      searchString: this.globalConstant.stringEmpty,
      paginationModel: {
        pageNumber: this.tableConstant.defaultPageIndexValue,
        pageSize: this.tableConstant.defaultPageSizeValue
      },
      items: [],
      filterItems: [],
      sortName: SortName.Id,
      sortType: SortType.asc
    };

    this.getFirstPage();
  }

  getServerData(event?: PageEvent): PageEvent {

    this.filterAuthor.paginationModel.pageNumber = event.pageIndex;
    this.filterAuthor.paginationModel.pageSize = event.pageSize;

    this.authorService.getPageAuthor(this.filterAuthor).subscribe(
      (response: FilterModel) => {
        if (this.filterAuthor.items == null) {
          return;
        }
        this.setDataOfServer(response);
      }
    );
    return event;
  }
  private setDataOfServer(filterModel: FilterModel) {
    this.authorList = filterModel.items;
    this.dataSource = new MatTableDataSource(this.authorList);
    this.length = filterModel.allCount;
  }
  delete(author: AuthorModel): void {

    const dialogRef = this.dialog.open(DialogTemplateBoolResultComponent, {
      data: {
        titleOk: this.dialogConstant.defaultDeleteTitleOk,
        titleCancel: this.dialogConstant.defaultDeleteTitleCancel,
        titleWindow: `Delete ${author.firstName + " " + author.lastName}`,
        textContent: this.dialogConstant.defaultDeleteTextContent
      }
    });
    dialogRef.afterClosed().subscribe((result: boolean) => {
      if (!result) {
        return;
      }
      this.authorService.deleteAuthor(author.id).subscribe(
        (response: BaseModel) => {
          if (response.errors.length != 0) {
            return;
          }
          location.reload();
        }
      );
    })
  }

  addAuthor(): void {
    let author: AuthorModel = {
      id: this.globalConstant.stringEmpty,
      firstName: this.globalConstant.stringEmpty,
      lastName: this.globalConstant.stringEmpty,
      printintEditions: [],
      errors: []
    };

    const dialogRef = this.dialog.open(DialogAuthorComponent, {
      width: this.dialogConstant.defaultDialogWidth,
      data: author
    });

    dialogRef.afterClosed().subscribe((result: AuthorModel) => {
      if (result == null) {
        return;
      }
      this.sendAddAuthor(result);
    });
  }

  private sendAddAuthor(author: AuthorModel): void {
    this.authorService.addAuthor(author).subscribe(
      (response: BaseModel) => {
        if (response.errors.length != 0) {
          return;
        }
        location.reload();
      }
    );

  }

  change(author: AuthorModel): void {
    const dialogRef = this.dialog.open(DialogAuthorComponent, {
      width: this.dialogConstant.defaultDialogWidth,
      data: author
    });
    dialogRef.afterClosed().subscribe((result: AuthorModel) => {
      if (result == null) {
        return;
      }
      this.sendUpdateAuthor(result);
    });
  }
  private sendUpdateAuthor(author: AuthorModel): void {
    this.authorService.updateAuthor(author).subscribe(
      (response: BaseModel) => {
        if (response.errors.length != 0) {
          return;
        }
        location.reload();
      }
    );
  }
}
