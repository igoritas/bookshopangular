import { Component, OnInit } from '@angular/core';
import { GlobalConstant } from 'src/app/sheared/global.constant';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  nameStore: string;
  constructor(public globalConstant: GlobalConstant) { }

  ngOnInit() {
    this.nameStore = this.globalConstant.nameSite;
  }

}
