import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { NotificationService } from 'src/app/services/notification.service';
import { GlobalConstant } from 'src/app/sheared/global.constant';
import { DialogInformationComponent } from './dialog-information/dialog-information.component';
import { BaseModel } from 'src/app/models/base/base.Model';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  submitted: boolean;
  regForm: FormGroup;
  constructor(
    private authenticationService: AuthenticationService,
    private routService: Router,
    private formBuilder: FormBuilder,
    private notificationService: NotificationService,
    public dialog: MatDialog,
    public globalConstant: GlobalConstant) {
    this.submitted = false;
    this.regForm = this.formBuilder.group({
      login: new FormControl(globalConstant.stringEmpty, Validators.required),
      firstName: new FormControl(globalConstant.stringEmpty, [Validators.required]),
      lastName: new FormControl(globalConstant.stringEmpty, Validators.required),
      email: new FormControl(globalConstant.stringEmpty, [Validators.required, Validators.email]),
      password: new FormControl(globalConstant.stringEmpty, [Validators.required, Validators.minLength(6)]),
      passwordConfirm: new FormControl(globalConstant.stringEmpty, [Validators.required, Validators.minLength(6)])
    })
  }

  ngOnInit() {
  }
  get f() { return this.regForm.controls; }

  submit(): void {
    this.submitted = true;
    if (this.regForm.invalid) {
      return;
    }
    this.authenticationService.registration(this.regForm.value).subscribe((date: BaseModel) => {
      if (date.errors.length != 0) {
        this.notificationService.showError(date.errors.toString());
        return;
      }
      this.dialog.open(DialogInformationComponent);
        this.routService.navigate(['/login']);
    });

  }

}
