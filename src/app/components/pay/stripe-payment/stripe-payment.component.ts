import { Component, ViewChild, ElementRef } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { FormGroup, FormBuilder, Validators } from "@angular/forms";

import { StripeService, Element as StripeElement, ElementsOptions, Elements } from "ngx-stripe";
import { StripeConstant } from 'src/app/sheared/stripe.constant';


@Component({
  selector: 'app-stripe-payment',
  templateUrl: './stripe-payment.component.html',
  styleUrls: ['./stripe-payment.component.scss']
})
export class StripePaymentComponent {
  elements: Elements;
  card: StripeElement;
  @ViewChild('card', { static: false }) cardRef: ElementRef;

  // optional parameters
  elementsOptions: ElementsOptions;

  stripeTest: FormGroup;

  constructor(
    private dialogRef: MatDialogRef<StripePaymentComponent>,
    private fb: FormBuilder,
    private stripeService: StripeService,
    private stripeConstaint: StripeConstant) {
    this.elementsOptions = stripeConstaint.elementsOptions;
  }

  ngOnInit() {
    this.stripeTest = this.fb.group({
      name: ['', [Validators.required]]
    });
    this.stripeService.elements(this.elementsOptions)
      .subscribe(elements => {
        this.elements = elements;
        // Only mount the element the first time
        if (!this.card) {
          this.card = this.elements.create('card', {
            style: {
              base: {
                iconColor: this.stripeConstaint.IconColor,
                color: this.stripeConstaint.Color,
                lineHeight: this.stripeConstaint.LineHeight,
                fontWeight: this.stripeConstaint.FontWeight,
                fontFamily: this.stripeConstaint.FontFamily,
                fontSize: this.stripeConstaint.FontSize,
                '::placeholder': {
                  color: this.stripeConstaint.PlaceHolderColor
                }
              }
            }
          });
          this.card.mount(this.cardRef.nativeElement);
        }
      });
  }

  buy() {
    const name = this.stripeTest.get('name').value;
    this.stripeService
      .createToken(this.card, { name })
      .subscribe(token => {
        debugger
        if (token.token) {
          // Use the token to create a charge or a customer
          // https://stripe.com/docs/charges
          console.log(token.token);
          this.dialogRef.close(token.token);
        } else if (token.error) {
          // Error creating the token
          console.log(token.error.message);
        }
      });
  }
}
