import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { CartService } from 'src/app/services/cart.service';
import { NotificationService } from 'src/app/services/notification.service';
import { OrderService } from 'src/app/services/order-service.service';
import { DialogConstant } from 'src/app/sheared/dialog.constant';
import { GlobalConstant } from 'src/app/sheared/global.constant';
import { StripePaymentComponent } from './stripe-payment/stripe-payment.component';
import {Currency,PayType} from '../../models/enums/index';
import {Order,OrderItems} from '../../models/order/index';
import { BaseModel } from 'src/app/models/base/base.Model';

@Component({
  selector: 'app-pay',
  templateUrl: './pay.component.html',
  styleUrls: ['./pay.component.scss']
})
export class PayComponent implements OnInit {
  bookOrder: Order;
  bookOrderItemList: OrderItems[];
  public currencyControl: FormControl;
  totaliPrice: number;
  summ: number
  currencySumm: string;
  currencyList;
  readonly defaultSumm: number = 0;

  constructor(
    private orderService: OrderService,
    private cartService: CartService,
    private routService: Router,
    public dialog: MatDialog,
    private notification: NotificationService,
    private globalConstant: GlobalConstant,
    public dialogConstant: DialogConstant) {

    this.bookOrder = {
      id: globalConstant.stringEmpty,
      currency: Currency.USD,
      date: new Date,
      isPaid: PayType.None,
      orderItems: [],
      totalPrice: 0,
      transactionId: globalConstant.stringEmpty,
      user: {
        id: globalConstant.stringEmpty,
        emailConfirmed: true,
        email: globalConstant.stringEmpty,
        firstName: globalConstant.stringEmpty,
        isActive: false,
        lastName: globalConstant.stringEmpty,
        orders: [],
        roles: [],
        userName: globalConstant.stringEmpty,
        errors: []
      },
      errors: []
    }
    this.summ = this.defaultSumm;
    this.currencyControl = new FormControl();
  }


  ngOnInit(): void {
    this.bookOrder.orderItems = this.cartService.getOrder();
    this.ifNotOrderItems()
    this.currencyList = this.globalConstant.currencyFilter;
  }
  private ifNotOrderItems(): void {
    if (this.bookOrder.orderItems.length == 0) {
      this.routService.navigate(['/home']);
    }
  }
  removeBook(item: OrderItems): void {
    this.cartService.removeBook(item);
    this.ifNotOrderItems();
  }
  addCountBook(item: OrderItems): void {
    let count = this.bookOrder.orderItems[this.bookOrder.orderItems.indexOf(item)].count;
    count++;
    this.summ += item.price;
    this.bookOrder.orderItems[this.bookOrder.orderItems.indexOf(item)].count = count;
  }

  removeCountBook(item: OrderItems): void {
    if (this.bookOrder.orderItems[this.bookOrder.orderItems.indexOf(item)].count != 1) {
      this.bookOrder.orderItems[this.bookOrder.orderItems.indexOf(item)].count -= 1;
      this.summ -= item.price;
    }
    if (this.bookOrder.orderItems[this.bookOrder.orderItems.indexOf(item)].count == 1) {
      this.removeBook(item);
    }
  }
  payOrder(): void {
    if (localStorage.getItem('email') == null) {
      this.routService.navigate(['/login']);
      return;
    }
    const dialogRef = this.dialog.open(StripePaymentComponent,
      {
        width: this.dialogConstant.defaultDialogWidth
      });
    dialogRef.afterClosed().subscribe(result => {
      if (!result) {
        return;
      }
      this.bookOrder.orderItems.forEach(element => {
        this.summ += element.price;
      });
      let orderResult: Order = {
        id: this.globalConstant.stringEmpty,
        user: {
          id: this.globalConstant.stringEmpty,
          emailConfirmed: true,
          errors: [],
          email: this.globalConstant.stringEmpty,
          firstName: this.globalConstant.stringEmpty,
          lastName: this.globalConstant.stringEmpty,
          isActive: false,
          orders: [],
          roles: [],
          userName: this.globalConstant.stringEmpty
        },
        currency: this.bookOrder.orderItems[0].book.currency,
        orderItems: this.bookOrder.orderItems,
        isPaid: PayType.Paid,
        totalPrice: this.summ,
        transactionId: result.card.id,
        date: new Date,
        errors: []
      };
      this.orderService.orderPayment(orderResult).subscribe((Response: BaseModel) => {
        if (Response.errors.length != 0) {
          Response.errors.forEach(element => {
            this.notification.showError(element);
          });
          return;
        }
        this.cartService.orderItemsList = [];
        this.cartService.count.next(0);
        this.routService.navigate(['/home']);
      })
    });
  }
}
