import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from "@angular/material/dialog";
import { FormControl, Validators, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-dialog-pay',
  templateUrl: './dialog-pay.component.html',
  styleUrls: ['./dialog-pay.component.scss']
})
export class DialogPayComponent implements OnInit {

  PayGroup: FormGroup = new FormGroup({
    "cart": new FormControl("", [Validators.required]),
  });
  constructor(
    private dialogRef: MatDialogRef<DialogPayComponent>) { }

  ngOnInit() {

  }
  onNoClick(): void {
    debugger
    this.dialogRef.close(this.PayGroup.value);
  }

}
