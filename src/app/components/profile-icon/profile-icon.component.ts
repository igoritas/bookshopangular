import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/app/services/authentication.service';

@Component({
  selector: 'app-profile-icon',
  templateUrl: './profile-icon.component.html',
  styleUrls: ['./profile-icon.component.scss']
})
export class ProfileIconComponent implements OnInit {
  fullNameUser: string;
  isSignIn: boolean;

  constructor(
    private authService: AuthenticationService
  ) {
    this.isSignIn = false;
  }

  ngOnInit() {
    this.authService.isSignInSubject.subscribe((date: boolean) => this.isSignIn = date);
    this.authService.fullNameUserSubject.subscribe((date: string) => this.fullNameUser = date);
  }
  LogOff(): void {
    this.authService.logOut().subscribe();

  }

}

