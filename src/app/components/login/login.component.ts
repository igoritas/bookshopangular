import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { DialogConstant } from 'src/app/sheared/dialog.constant';
import { GlobalConstant } from 'src/app/sheared/global.constant';
import { CookiHelper } from 'src/app/helpers/cooki.helpers';
import { ForwardPasswordComponent } from './forward-password/forward-password.component';
import { StorageHelper } from 'src/app/helpers/storage.helpers';
import {LoginModel} from '../../models/user/index';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  user: LoginModel;
  loginForm: FormGroup;
  constructor(
    private authenticationService: AuthenticationService,
    private cookieHelper: CookiHelper,
    private routService: Router,
    public dialog: MatDialog,
    public globalConstant: GlobalConstant,
    public dialogConstant: DialogConstant,
    public storageHelper:StorageHelper
  ) {
    this.loginForm = new FormGroup({
      login: new FormControl(globalConstant.stringEmpty, Validators.required),
      password: new FormControl(globalConstant.stringEmpty, [Validators.required]),
      isRememberMe: new FormControl(globalConstant.stringEmpty)
    });

  }

  ngOnInit() {
    this.storageHelper.clear();
    this.cookieHelper.deleteAllCookie();
  }

  submit(): void {
    this.user = {
      login: this.loginForm.controls.login.value,
      password: this.loginForm.controls.password.value
    }

    this.authenticationService.logIn(this.user).subscribe(date => {
      if (!date) {
      return;  
      }
      this.authenticationService.setUserToLocalStorag(date);
      this.routService.navigate(['/home']);
    });

    if (!this.loginForm.controls.isRememberMe.value) {
      let allCookie = this.cookieHelper.getAllCookie();
      this.cookieHelper.setCookieForExpire(
        [this.globalConstant.accessToken, this.globalConstant.refreshToken],
        [allCookie[this.globalConstant.accessToken],
          allCookie[this.globalConstant.refreshToken]],
           this.globalConstant.dateExpireIsNotRemember);
    }

  }
  forwardPassword(): void {
    const dialogRef = this.dialog.open(ForwardPasswordComponent, {
      width: this.dialogConstant.defaultDialogWidth,
      height: this.dialogConstant.defaultDialogHeight
    });
    dialogRef.afterClosed().subscribe((result: string) => {
      if (!result) {
        return;
      }
      this.authenticationService.forwardPassword(result).subscribe();
    })
  }

}
