import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-forward-password',
  templateUrl: './forward-password.component.html',
  styleUrls: ['./forward-password.component.scss']
})
export class ForwardPasswordComponent implements OnInit {
  email: string;
  constructor(private dialogRef: MatDialogRef<ForwardPasswordComponent>) { }

  ngOnInit() {
  }
  onNoClick() {
    this.dialogRef.close(this.email);
  }
}
