import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-dialog-discription',
  templateUrl: './dialog-discription.component.html',
  styleUrls: ['./dialog-discription.component.scss']
})
export class DialogDiscriptionComponent implements OnInit {

  constructor(
    private dialogRef: MatDialogRef<DialogDiscriptionComponent>,
    @Inject(MAT_DIALOG_DATA) public data: string
  ) { }

  ngOnInit() {
  }
  onNoClick() {
    debugger
    this.dialogRef.close(this.data);
  }
}
