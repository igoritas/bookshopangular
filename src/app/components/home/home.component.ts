import { animate, state, style, transition, trigger } from '@angular/animations';
import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { PageEvent } from '@angular/material/paginator';
import { PrintingEditionService } from 'src/app/services/printingEdition.service';
import { OrderService } from 'src/app/services/order-service.service';
import { CartService } from '../../services/index';
import { GlobalConstant } from '../../sheared/global.constant';
import { DialogConstant, TableConstant } from '../../sheared/index';
import { SortType } from 'src/app/models/enums/sortType';
import { PrintingEditionFilter } from 'src/app/models/filter/printingEditionFilter.Model';
import { FilterModel } from 'src/app/models/filter/filter.Model';
import { SortPrintingEdition } from 'src/app/models/filter/sortPrintingEdition.Model';
import { SortName } from 'src/app/models/enums/sortName';
import { CurrencyFilter } from 'src/app/models/filter/currencyFilter.Model';
import { OrderItems } from 'src/app/models/order/orderItem.Model';
import { PrintingEdition } from 'src/app/models/printingEdition/printingEdition.Model';
import { Toggle } from 'src/app/models/filter/toggle.Model';




@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
 
})
export class HomeComponent implements OnInit {
  currencyControl: FormControl;
  currencySumm: string;
  currencyList:CurrencyFilter[];

  private readonly  defaultMaxCountOrderItem: number = 100;
  private readonly defaultCountChacked: number = this.globalConstant.numberEmpty;

  orderItemList: OrderItems[];
  bookList: PrintingEdition[];
  filterBookData: PrintingEditionFilter;
  isChecked: boolean;
  toggleList: Toggle[];
  sortList: SortPrintingEdition[];

  resizeOptionTable: number[];

  defaultMinPrice: number;
  defaultMaxPrice: number;

  myUrl: string;
  pageEvent: PageEvent;
  pageIndex: number;
  pageSize: number;
  length: number;

  constructor(private bookService: PrintingEditionService,
    private orderService: OrderService,
    private cartService: CartService,
    public dialog: MatDialog,
    public globalConstant: GlobalConstant,
    public tableConstant: TableConstant,
    public dialogConstant: DialogConstant
  ) {
    this.currencyControl = new FormControl();
    this.currencyList = globalConstant.currencyFilter;
    this.currencyControl.setValue(globalConstant.numberEmpty);
    this.orderItemList = [];
    this.bookList = [];
    this.isChecked = true;
    this.defaultMaxPrice = globalConstant.numberEmpty;
    this.defaultMinPrice = globalConstant.numberEmpty;
    this.resizeOptionTable = this.tableConstant.defaultResizeOptionValue;

    this.toggleList = this.globalConstant.toggleBook;
    this.sortList = this.globalConstant.sortBookUserPresentetion;
    this.filterBookData = {
      minPrice: this.defaultMinPrice,
      maxPrice: this.defaultMaxPrice,
      allCount: this.globalConstant.numberEmpty,
      searchString: this.globalConstant.stringEmpty,
      paginationModel: {
        pageNumber: this.tableConstant.defaultPageIndexValue,
        pageSize: this.tableConstant.defaultPageSizeValue

      },
      items: [],
      filterItems: [],
      sortName: SortName.Price,
      sortType: SortType.asc
    };
  }

  ngOnInit(): void {
    this.getFirstPage()
  }

  private getFirstPage(): void {
    let firstPage = new PageEvent();
    firstPage.pageIndex = this.tableConstant.defaultPageIndexValue;
    firstPage.pageSize = this.filterBookData.paginationModel.pageSize;
    this.getFilteredData(firstPage);
  }

  filterMinPrice(filterValue: KeyboardEvent): void {
    let value = (filterValue.target as HTMLInputElement).value;
    if (+value > this.filterBookData.maxPrice) {
      (filterValue.target as HTMLInputElement).value = this.filterBookData.minPrice.toString();
      return;
    }
    this.filterBookData.minPrice = +value;
  }

  filterMaxPrice(filterValue: KeyboardEvent): void {
    let value = (filterValue.target as HTMLInputElement).value;
    if (+value < this.filterBookData.minPrice) {
      (filterValue.target as HTMLInputElement).value = this.filterBookData.maxPrice.toString();
      return;
    }
    this.filterBookData.maxPrice = +value;
  }

  searchFilter(filterValue: string): void {
    this.filterBookData.searchString = filterValue;
  }

  getFilteredData(event?: PageEvent): PageEvent {
    this.filterBook();
    this.filterBookData.paginationModel.pageNumber = event.pageIndex;
    this.sendGetFilter();
    this.filterBookData.filterItems = [];
    return event;
  }

  private sendGetFilter(): void {
    this.bookService.getFiltredBookData(this.filterBookData).subscribe(
      (response: FilterModel) => {
        if (this.filterBookData.items == null) {
          return;
        }
        this.bookList = response.items;
        response.items.forEach(element => {
          this.orderItemList.push({
            book: element,
            count: this.globalConstant.numberEmpty,
            id: this.globalConstant.stringEmpty,
            price: this.globalConstant.numberEmpty
          })
        });
        this.length = response.allCount;
        this.changeSelect();
      }
    );
  }

  changeSelect(): void {
    this.changValueSummOfCarency(this.currencyControl.value);
  }

  changValueSummOfCarency(currency: number): void {
    this.bookList.forEach((element: PrintingEdition) => {
      if (element.currency.toString() == this.currencyList[currency].currency) {
        return;
       }
       this.getCoefficientCurrency(element, currency);
    });
  }

  private getCoefficientCurrency(element: PrintingEdition, currency: number) : void{
    this.orderService.ConvertCurrency(element.currency, this.currencyList[currency].value).subscribe((data:any) => {
      element.currency = this.currencyList[currency].value;
      element.price = element.price * data.result;
    }
    );
  }

  addToCart(book: PrintingEdition): void {
    this.cartService.addBook(this.orderItemList[this.bookList.indexOf(book)]);
  }


  changeCountItem(book, count): void {
    let index = this.bookList.indexOf(book);
    if (this.defaultMaxCountOrderItem > count.target.value) {
      this.orderItemList[index].count = count.target.value;
    }
    if (this.defaultMaxCountOrderItem < count.target.value) {
      count.target.value = this.defaultMaxCountOrderItem;
    }
  }

  checkValue(event: any, index: number): void {
    let CountChacked = this.defaultCountChacked;
    for (let index = 0; index < this.toggleList.length; index++) {
      if (!this.toggleList[index].isCheck) {
        continue;
      }
      CountChacked++;
    }
    if (CountChacked != this.defaultCountChacked) {
      return;      
    }
    this.toggleList[index].isCheck = true;
      event.source.toggle();
  }

  sortType(event): void {
    this.filterBookData.sortType = this.sortList[event.target.value].value;
    this.getFirstPage();
  }

  filterBook(): void {
    for (let index = 0; index < this.toggleList.length; index++) {
      if (this.toggleList[index].isCheck) {
        continue;
      }
      this.filterBookData.filterItems.push({
        columnName: this.toggleList[index].name,
        value: this.toggleList[index].value
      });
    }
  }
}

