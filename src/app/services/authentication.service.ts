import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { environment } from 'src/environments/environment.prod';
import { CookiHelper } from 'src/app/helpers/cooki.helpers';
import { StorageHelper } from 'src/app/helpers/storage.helpers';
import { GlobalConstant } from '../sheared';
import { User } from '../models/user/user.Model';
import { LoginModel } from '../models/user/login.Model';
import { RegistrationModel } from '../models/user/registrationModel';
import { BaseModel } from '../models/base/base.Model';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  isSignInSubject: BehaviorSubject<boolean>;
  fullNameUserSubject: BehaviorSubject<string>;

  constructor(
    private httpClient: HttpClient,
    private cookieHelper: CookiHelper,
    private globalConstant: GlobalConstant,
    private storageHelper: StorageHelper
  ) {
    this.fullNameUserSubject = new BehaviorSubject(globalConstant.stringEmpty);
    this.isSignInSubject = new BehaviorSubject(false);
    if (storageHelper.getByKey(this.globalConstant.firstNameOfStorage) != null && storageHelper.getByKey(this.globalConstant.firstNameOfStorage) != null) {
      let fullName = storageHelper.getByKey(this.globalConstant.firstNameOfStorage) + this.globalConstant.stringSpace + storageHelper.getByKey(this.globalConstant.lastNameOfStorage);
      this.chengSubject(fullName, true);
    }
  }

  setUserToLocalStorag(user: User): void {
    this.storageHelper.setItem(this.globalConstant.firstNameOfStorage, user.firstName);
    this.storageHelper.setItem(this.globalConstant.lastNameOfStorage, user.lastName);
    this.storageHelper.setItem(this.globalConstant.emailOfStorage, user.email);
    this.storageHelper.setItem(this.globalConstant.roleOfStorage, user.roles[0]);

    let fullName = user.firstName + this.globalConstant.stringSpace + user.lastName;
    this.chengSubject(fullName, true);
  }
  logIn(user: LoginModel): Observable<User> {
    return this.httpClient.post<User>(`${environment.apiUrl}/api/account/Login`, user, { withCredentials: true });
  }

  logOut(): Observable<any> {
    this.cookieHelper.deleteAllCookie();
    this.chengSubject(this.globalConstant.stringEmpty, false);
    return this.httpClient.post<any>(`${environment.apiUrl}/api/account/LogOff`, {});
  }

  private chengSubject(fullName: string, state: boolean): void {
    this.isSignInSubject.next(state);
    this.fullNameUserSubject.next(fullName);
  }

  registration(user: RegistrationModel): Observable<BaseModel> {
    return this.httpClient.post<BaseModel>(`${environment.apiUrl}/api/account/Register`, user);
  }

  refreshToken(token: string, refreshToken: string): Observable<any> {
    return this.httpClient.post<any>(`${environment.apiUrl}/api/account/Refresh`, { accessToken: token, refreshToken: refreshToken }, { withCredentials: true });
  }

  forwardPassword(email: string): Observable<any> {
    return this.httpClient.get(`${environment.apiUrl}/api/account/ForgotPassword/${email}`);
  }

  logError(message: string): void {
    console.log('LoggingService: ' + message);
  }

}