import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { PrintingEdition } from '../models/printingEdition/printingEdition.Model';
import { BaseModel } from '../models/base/base.Model';
import { FilterModel } from '../models/filter/filter.Model';



@Injectable({
  providedIn: 'root'
})
export class PrintingEditionService {
  constructor(private http: HttpClient) { }
  update(book: PrintingEdition): Observable<BaseModel> {
    return this.http.put<BaseModel>(`${environment.apiUrl}/api/PrintingEdition/Update`, book);
  }
  getFiltredBookData(indexModel: FilterModel): Observable<FilterModel> {
    return this.http.post<FilterModel>(`${environment.apiUrl}/api/PrintingEdition/GetAll`, indexModel);
  }
  addBook(book: PrintingEdition): Observable<BaseModel> {
    return this.http.post<BaseModel>(`${environment.apiUrl}/api/PrintingEdition/Add`, book);
  }
  deletebook(book: string): Observable<BaseModel> {
    return this.http.delete<BaseModel>(`${environment.apiUrl}/api/PrintingEdition/Delete/${book}`);
  }
}
