import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { AuthorModel } from '../models/author/author.Model';
import { BaseModel } from '../models/base/base.Model';
import { FilterModel } from '../models/filter/filter.Model';

@Injectable({
  providedIn: 'root'
})
export class AuthorService {

  constructor(private http: HttpClient) { }

  getAuthors(): Observable<AuthorModel[]> {
    return this.http.get<AuthorModel[]>(`${environment.apiUrl}/api/author/GetAll`);
  }
  deleteAuthor(athorId: string): Observable<BaseModel> {
    return this.http.delete<BaseModel>(`${environment.apiUrl}/api/Author/Delete/${athorId}`);
  }
  addAuthor(athor: AuthorModel): Observable<BaseModel> {
    return this.http.post<BaseModel>(`${environment.apiUrl}/api/Author/Add`, athor);
  }
  updateAuthor(athor: AuthorModel): Observable<BaseModel> {
    return this.http.put<BaseModel>(`${environment.apiUrl}/api/Author/Update`, athor);
  }
  getPageAuthor(indexModel: FilterModel): Observable<FilterModel> {
    return this.http.post<FilterModel>(`${environment.apiUrl}/api/author/GetFilteringData`, indexModel);
  }
}
