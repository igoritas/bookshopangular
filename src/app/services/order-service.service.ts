import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { FilterModel } from '../models/filter/filter.Model';
import { Currency } from '../models/enums';
import { Order } from '../models/order/order.Model';
import { BaseModel } from '../models/base/base.Model';
@Injectable({
    providedIn: 'root'
})
export class OrderService {

    constructor(private http: HttpClient) { }

    orderPayment(order: Order): Observable<BaseModel> {
        return this.http.post<BaseModel>(`${environment.apiUrl}/api/order/Add`, order);
    }
    ConvertCurrency(base: Currency, rate: Currency): Observable<any> {
        return this.http.get<any>(`${environment.apiUrl}/api/order/ConvertCurrency/${base}/${rate}`);
    }
    getFiltredOrderData(indexModel: FilterModel): Observable<FilterModel> {
        return this.http.post<FilterModel>(`${environment.apiUrl}/api/order/GetAll`, indexModel);
    }
}
