import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { OrderItems } from '../models/order/orderItem.Model';
@Injectable({
    providedIn: 'root'
})
export class CartService {
    orderItemsList: OrderItems[] = [];
    count: Subject<number> = new Subject<number>();
    constructor() { }
    getOrder(): OrderItems[] {
        return this.orderItemsList;
    }
    addBook(oprderItem: OrderItems): void {
        let orderItem: OrderItems = {
            id: "",
            book: oprderItem.book,
            count: +oprderItem.count,
            price: oprderItem.book.price * oprderItem.count
        };
        this.orderItemsList.push(orderItem);
        this.count.next(this.orderItemsList.length);
    }
    removeBook(book): void {
        this.orderItemsList.splice(this.orderItemsList.indexOf(book), 1);
        this.count.next(this.orderItemsList.length);
    }

}
