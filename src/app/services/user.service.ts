import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { FilterModel } from '../models/filter/filter.Model';
import { BaseModel } from '../models/base/base.Model';
import { EditUserModel } from '../models/user/editUser.Model';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  constructor(private http: HttpClient) { }
  getFiltredUsersData(indexModel: FilterModel): Observable<FilterModel> {
    return this.http.post<FilterModel>(`${environment.apiUrl}/api/user/GetAll`, indexModel);
  }
  deleteUser(id: string): Observable<BaseModel> {
    return this.http.delete<BaseModel>(`${environment.apiUrl}/api/user/Delete/${id}`);
  }
  updateUser(user: EditUserModel): Observable<BaseModel> {
    return this.http.put<BaseModel>(`${environment.apiUrl}/api/user/Update`, user);
  }

}
