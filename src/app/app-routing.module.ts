import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { PayComponent } from './components/pay/pay.component';
import { ProfileComponent } from './components/profile/profile.component';
import { UserGuard } from './guards/user.guard';


const routes: Routes = [
  {
    path: '',
    canActivate: [UserGuard],
    children: [
      { path: 'home', component: HomeComponent, pathMatch: 'full' }
    ]
  },
  {
    path: '',
    children: [
      { path: 'login', component: LoginComponent, pathMatch: 'full' }
    ]
  },
  {
    path: '',
    children: [
      { path: 'register', component: RegisterComponent, pathMatch: 'full' }
    ]
  },
  {
    path: '',
    children: [
      { path: 'profile', component: ProfileComponent, pathMatch: 'full' }
    ]
  },
  {
    path: '',
    children: [
      { path: 'pay', component: PayComponent, pathMatch: 'full' }
    ]
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
