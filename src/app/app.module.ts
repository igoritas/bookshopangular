import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";

import { HomeComponent } from "./components/home/home.component";
import { PrintingEditionService } from "./services/printingEdition.service";
import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";
import { HttpModule } from "@angular/http";
import { HeaderComponent } from "./components/header/header.component";
import { LoginComponent } from "./components/login/login.component";

import { CookieService } from "ngx-cookie-service";
import { AuthenticationService } from "./services/authentication.service";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RegisterComponent } from "./components/register/register.component";
import { CartIconComponent } from "./components/cart-icon/cart-icon.component";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";
import { MatBadgeModule } from "@angular/material/badge";
import { MatIconModule } from "@angular/material/icon";
import { MatSidenavModule } from "@angular/material/sidenav";
import { MatTableModule } from "@angular/material/table";


import { MatDialogModule } from "@angular/material/dialog";
import { GlobalErrorHandler } from "src/app/helpers/global-error-hendler";
import { JwtInterceptor } from "src/app/intercepter/jwt.Interceptor";
import { MatSnackBarModule } from "@angular/material/snack-bar";
import { ErrorService } from "./services/error.service";
import { NotificationService } from "./services/notification.service";
import { PayComponent } from "./components/pay/pay.component";
import { MatPaginatorModule } from "@angular/material/paginator";
import { AuthorService } from './services/author.service';
import { MatSelectModule } from '@angular/material/select';
import { A11yModule } from '@angular/cdk/a11y';
import { CdkTableModule } from '@angular/cdk/table';
import { MatNativeDateModule, MatRippleModule } from '@angular/material/core';
import { MatSortModule } from '@angular/material/sort';
import { PortalModule } from '@angular/cdk/portal';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { MatFormFieldModule, MatInputModule } from '@angular/material';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';

import { DatePipe } from '@angular/common';


import { DialogPayComponent } from './components/pay/dialog-pay/dialog-pay.component';
import { DialogDiscriptionComponent } from './components/home/dialog-discription/dialog-discription.component';

import { NgxStripeModule } from 'ngx-stripe';

import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { AdminModule } from './module/admin/admin.module';
import { AdminRoutingModule } from './module/admin/admin-routing.module';
import { StripePaymentComponent } from './components/pay/stripe-payment/stripe-payment.component';
import { ForwardPasswordComponent } from './components/login/forward-password/forward-password.component';
import { DialogTemplateBoolResultComponent } from './components/admin/dialogs/dialog-template-bool-result/dialog-template-bool-result.component';
import { ProfileIconComponent } from './components/profile-icon/profile-icon.component';
import { ProfileComponent } from './components/profile/profile.component';
import { DialogInformationComponent } from './components/register/dialog-information/dialog-information.component';
import { UserGuard } from './guards/user.guard';
import { CookiHelper } from 'src/app/helpers/cooki.helpers';
import { PipeModule } from './module/pipe/pipe.module';
import { GlobalConstant } from './sheared/global.constant';
import { TableConstant } from './sheared/table.constant';
import { DialogConstant } from './sheared/dialog.constant';
import { StorageHelper } from 'src/app/helpers/storage.helpers';
import { StripeConstant } from './sheared/stripe.constant';



@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    HeaderComponent,
    LoginComponent,
    RegisterComponent,
    CartIconComponent,
    PayComponent,
    DialogPayComponent,
    DialogDiscriptionComponent,
    StripePaymentComponent,
    ForwardPasswordComponent,
    DialogTemplateBoolResultComponent,
    ProfileIconComponent,
    ProfileComponent,
    DialogInformationComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    HttpModule,
    PipeModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    FontAwesomeModule,
    A11yModule,
    NgxStripeModule.forRoot('pk_test_W4k3WJkT9sRRNdKBzwio1m0A00nNLq4RY9'),
    CdkTableModule,
    MatBadgeModule,
    MatDialogModule,
    MatIconModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSnackBarModule,
    MatSortModule,
    MatCheckboxModule,
    MatTableModule,
    PortalModule,
    ScrollingModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatSlideToggleModule,
    AdminModule,
    AdminRoutingModule
  ],

  providers: [
    PrintingEditionService,
    CookieService,
    ErrorService,
    NotificationService,
    AuthenticationService,
    GlobalErrorHandler,
    AuthorService,
    StripeConstant,
    CookiHelper,
    StorageHelper,
    DatePipe,
    UserGuard,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: JwtInterceptor,
      multi: true
    },
    GlobalConstant,
    TableConstant,
    DialogConstant
  ],
  entryComponents: [
    DialogPayComponent,
    DialogDiscriptionComponent,
    StripePaymentComponent,
    ForwardPasswordComponent,
    DialogInformationComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
