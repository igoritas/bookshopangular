import { Injectable } from '@angular/core';


@Injectable()
export class DialogConstant { 
    public readonly defaultDeleteTitleOk = "Delete" ;
    public readonly defaultDeleteTitleCancel = "Cancel" ;
    public readonly defaultDeleteTextContent = "You sure?" ;
    public readonly defaultLogForClosed = "The dialog was closed";
    public readonly defaultDialogWidth = "500px";
    public readonly defaultDialogHeight = "600px";
}