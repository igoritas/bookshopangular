import { Injectable } from '@angular/core';
import { SortType } from '../models/enums/sortType'
import { SortPrintingEdition } from '../models/filter/sortPrintingEdition.Model';
import { FilterColumnName, SortName, Currency } from '../models/enums';
import { Toggle } from '../models/filter/toggle.Model';
import { CurrencyFilter } from '../models/filter/currencyFilter.Model';

@Injectable()
export class GlobalConstant {
  public readonly toggleBook:Toggle[] = [{
    name: FilterColumnName.PrintingEditionType,
    textToggle: "Book",
    value: 0,
    isCheck: true
  },
  {
    name: FilterColumnName.PrintingEditionType,
    textToggle: "Journal",
    value: 1,
    isCheck: true
  },
  {
    name: FilterColumnName.PrintingEditionType,
    textToggle: "Newspaper",
    value: 2,
    isCheck: true
  }
  ];
  public readonly sortBookUserPresentetion:SortPrintingEdition[] = [{
    nameSort: SortName.Price,
    textSort: "Price Ascending",
    value: SortType.asc
  },
  {
    nameSort: SortName.Price,
    textSort: "Price Descending",
    value: SortType.desc
  },
  ]
  public readonly togglOreder: Toggle[] = [
    {
      name: FilterColumnName.IsPaid,
      textToggle: "Unpaid",
      value: 1,
      isCheck: true
    },
    {
      name: FilterColumnName.IsPaid,
      textToggle: "Paid",
      value: 2,
      isCheck: true
    }
  ];
  public readonly toggleActive:Toggle[] = [
    {
      name: FilterColumnName.IsActive,
      textToggle: "Blocked",
      value: 0,
      isCheck: true
    },
    {
      name: FilterColumnName.IsActive,
      textToggle: "Active",
      value: 1,
      isCheck: true
    }
  ];
  public readonly printingEditionString:string[] = [
   "Book",
    "Journal",
   "Newspaper",
  ]
  public readonly currencyFilter:CurrencyFilter[] = [{
    currency: "USD",
    value: Currency.USD
  },
  {
    currency: "UAH",
    value: Currency.UAH
  },
  {
    currency: "EUR",
    value: Currency.EUR
  },
  {
    currency: "RUB",
    value: Currency.RUB
  }
  ]

  

public readonly stringEmpty:string = "";
public readonly stringSpace:string =" ";

public readonly nameSite:string ="Book Shop";

public readonly accessToken: string = "asseccToken";
public readonly refreshToken: string = "asseccToken";

public readonly accessRoleAdminPanel: string = "admin";

public readonly errorAccess:number = 401;

public readonly numberEmpty:number = 0;

public readonly dateExpireIsNotRemember:Date =new Date(0, 0, 0, 12, 0, 0, 0);

public readonly firstNameOfStorage:string = "firstName";
public readonly lastNameOfStorage:string = "lastname";
public readonly emailOfStorage:string = "email";
public readonly roleOfStorage:string = "Role";

public readonly errorMessageAccessAdmin:string = "Opps, do you really think go to admin panel ?))";
public readonly infoMessageAccessAdmin:string ="Have a nice day";

}