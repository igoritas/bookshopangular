import { Injectable } from '@angular/core';
import { ElementsOptions } from "ngx-stripe";


@Injectable()
export class StripeConstant {
    public readonly IconColor:string = "#666EE8";
    public readonly Color:string = "#fff";
    public readonly LineHeight:string = "40px";
    public readonly FontWeight:number = 300;
    public readonly FontFamily:string = '"Helvetica Neue", Helvetica, sans-serif';
    public readonly FontSize:string = '18px';
    public readonly PlaceHolderColor:string = '#CFD7E0';
    public readonly elementsOptions: ElementsOptions = {
        locale:'auto'
    };
}