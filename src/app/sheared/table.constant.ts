import { Injectable } from '@angular/core';


@Injectable()
export class TableConstant { 
    
  public readonly displayedColumnPrintingEdition = [
    "Id",
    "name",
    "discription",
    "type",
    "authors",
    "Price",
    "customColumn"
  ];
  public readonly displayedColumnAuthor = [
    "Id",
    "Name",
    "PrintingEdition",
    "customColumn"
  ];
  public readonly displayedColumnOrder = [
    "Id",
    "CreationDate",
    "UserName",
    "UserEmail",
    "Product",
    "Title",
    "OrderPrice",
  ];
  public readonly diplayedColumnUser = [
    "firstName",
    "lastName",
    "codeEmail",
    "email",
    "isActive",
    "customColumn"
  ];
  public readonly defaultResizeOptionValue = [5, 10, 15];
  public readonly defaultPageSizeValue = 5;
  public readonly defaultPageIndexValue = 0;
}