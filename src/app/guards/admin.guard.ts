import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { StorageHelper } from 'src/app/helpers/storage.helpers';
import { GlobalConstant } from '../sheared';

@Injectable()
export class AdminGuard implements CanActivate {
    private accessRole: string;
    constructor(
        private rout: Router,
        private storageHelper: StorageHelper,
        private globalConstant: GlobalConstant) {
        this.accessRole = globalConstant.accessRoleAdminPanel;
    }
    canActivate(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ): Observable<boolean> | Promise<boolean> | boolean {
        if (this.storageHelper.getByKey(this.globalConstant.roleOfStorage) == this.accessRole) {
            return true;
        }
        if (confirm(this.globalConstant.errorMessageAccessAdmin)) {
            alert(this.globalConstant.infoMessageAccessAdmin);
            this.rout.navigate(['/login']);
        }
        return false;
    }

}