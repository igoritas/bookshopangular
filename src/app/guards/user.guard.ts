import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { StorageHelper } from 'src/app/helpers/storage.helpers';
import { GlobalConstant } from '../sheared';

@Injectable({ providedIn: 'root' })
export class UserGuard implements CanActivate {
    private unsuccessfulRole: string;
    constructor(private storageHelper: StorageHelper
        , private rout: Router,
        private globalConstant: GlobalConstant) {
        this.unsuccessfulRole = globalConstant.accessRoleAdminPanel;
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        if (this.storageHelper.getByKey(this.globalConstant.roleOfStorage) == this.unsuccessfulRole) {
            this.rout.navigate(['/admin/book']);
            return false;
        }
        return true;
    }
}