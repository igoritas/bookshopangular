import { NgModule } from '@angular/core';
import { ENumAsStringCurrency } from '../../helpers/custom-pipe/enum-to-string-currency-pipe';
import { ENumAsStringPipe } from '../../helpers/custom-pipe/enum-to-string-pipe';

@NgModule({
    imports: [],
    exports: [ENumAsStringPipe,
        ENumAsStringCurrency],
    declarations: [
        ENumAsStringPipe,
        ENumAsStringCurrency
    ],
    providers: [],
})
export class PipeModule { }
