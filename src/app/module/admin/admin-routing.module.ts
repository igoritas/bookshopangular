import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminGuard } from 'src/app/guards/admin.guard';
import { AdminComponent } from '../../components/admin/admin.component';
import { AuthorComponent } from '../../components/admin/author/author.component';
import { OrderComponent } from '../../components/admin/order/order.component';
import { UserComponent } from '../../components/admin/user/user.component';

const routes2: Routes = [
  {
    path: 'admin',
    canActivate: [AdminGuard],
    children: [
      { path: 'book', component: AdminComponent, pathMatch: 'full' },
      { path: 'author', component: AuthorComponent, pathMatch: 'full' },
      { path: 'order', component: OrderComponent, pathMatch: 'full' },
      { path: 'user', component: UserComponent, pathMatch: 'full' }
    ]
  }
]
@NgModule({
  imports: [RouterModule.forChild(routes2)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
