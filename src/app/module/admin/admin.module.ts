import { A11yModule } from '@angular/cdk/a11y';
import { PortalModule } from '@angular/cdk/portal';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { CdkTableModule } from '@angular/cdk/table';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import {
  MatBadgeModule,
  MatButtonModule,
  MatCheckboxModule,
  MatDialogModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatRippleModule,
  MatSelectModule,
  MatSidenavModule,
  MatSlideToggleModule,
  MatSnackBarModule,
  MatSortModule,
  MatTableModule
} from '@angular/material';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { AppRoutingModule } from 'src/app/app-routing.module';
import { DialogOverviewExampleDialog } from 'src/app/components/admin/dialogs/dialog-printing-edition/dialog-overview-example-dialog';
import { AdminGuard } from 'src/app/guards/admin.guard';
import { AdminComponent } from "../../components/admin/admin.component";
import { AuthorComponent } from '../../components/admin/author/author.component';
import { DialogAuthorComponent } from '../../components/admin/dialogs/dialog-author/dialog-author.component';
import { DialogTemplateBoolResultComponent } from '../../components/admin/dialogs/dialog-template-bool-result/dialog-template-bool-result.component';
import { DialogUserComponent } from '../../components/admin/dialogs/dialog-user/dialog-user.component';
import { OrderComponent } from '../../components/admin/order/order.component';
import { UserComponent } from '../../components/admin/user/user.component';
import { PipeModule } from '../pipe/pipe.module';
import { AdminRoutingModule } from './admin-routing.module';






@NgModule({
  declarations: [
    AdminComponent,
    DialogOverviewExampleDialog,
    DialogAuthorComponent,
    AuthorComponent,
    OrderComponent,
    UserComponent,
    DialogUserComponent
  ],
  imports: [
    CommonModule,
    AdminRoutingModule,
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    FontAwesomeModule,
    MatBadgeModule,
    MatAutocompleteModule,
    MatCheckboxModule,
    MatIconModule,
    MatSidenavModule,
    MatTableModule,
    MatDialogModule,
    PipeModule,
    MatSnackBarModule,
    MatSelectModule,
    MatPaginatorModule,
    A11yModule,
    CdkTableModule,
    MatBadgeModule,
    MatDialogModule,
    MatIconModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSnackBarModule,
    MatSortModule,
    MatTableModule,
    PortalModule,
    ScrollingModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatSlideToggleModule,
  ],
  providers: [AdminGuard],
  entryComponents: [
    DialogOverviewExampleDialog,
    DialogAuthorComponent,
    DialogUserComponent,
    DialogTemplateBoolResultComponent
  ],

})
export class AdminModule { }
