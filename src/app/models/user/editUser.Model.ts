import { BaseModel } from '../base/base.Model';

export interface EditUserModel extends BaseModel {
    firstName: string;
    lastName: string;
    email: string;
    oldPassword: string;
    newPassword: string;
}