import { BaseModel } from "../base/base.Model";
import { Order } from '../order/order.Model';

export interface User extends BaseModel {
    id: string;
    firstName: string;
    lastName: string;
    userName: string;
    emailConfirmed: boolean;
    email: string;
    isActive: boolean;
    roles: string[];
    orders: Order[];
}