import { BaseModel } from "../base/base.Model";

export interface RegistrationModel extends BaseModel {
    login: string;
    email: string;
    firstName: string;
    lastName: string;
    password: string;
    passwordConfirm: string;
}