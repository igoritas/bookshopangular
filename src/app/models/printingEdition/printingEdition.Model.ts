import { BaseModel } from "../base/base.Model";
import { PrinringEditionType, Currency } from '../enums';
import { AuthorModel } from '../author/author.Model';

export interface PrintingEdition extends BaseModel {
  id: string;
  name: string;
  type: PrinringEditionType;
  price: number;
  currency: Currency;
  publicationDate: Date;
  authors?: AuthorModel[];
  discription: string;
}