import { BaseModel } from '../base/base.Model';
import { PrintingEdition } from '../printingEdition/printingEdition.Model';

export interface AuthorModel extends BaseModel {
  id: string;
  firstName: string;
  lastName: string;
  printintEditions: PrintingEdition[];
}