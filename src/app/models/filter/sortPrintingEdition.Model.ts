import { SortType } from "../enums/sortType";
import { SortName } from '../enums';

export interface SortPrintingEdition {
    nameSort: SortName,
    textSort: string,
    value: SortType
}