import { Currency } from '../enums';

export interface CurrencyFilter {
    currency: string,
    value: Currency
}