import { FilterModel } from "./filter.Model";

export interface PrintingEditionFilter extends FilterModel {
    minPrice: number;
    maxPrice: number;
}