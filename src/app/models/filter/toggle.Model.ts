import { FilterColumnName } from '../enums';

export interface Toggle {
    name: FilterColumnName,
    textToggle: String,
    value: number,
    isCheck: boolean
}