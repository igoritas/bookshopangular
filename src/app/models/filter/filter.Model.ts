import { SortType } from "../enums/sortType";
import { SortName } from '../enums';
import { PaginationModel } from './pagination.Model';
import { FilterCheckBox } from './filterCheckBox.Model';

 export interface FilterModel {
    searchString: string,
    items: any,
    allCount: number,
    sortName: SortName,
    sortType: SortType
    paginationModel: PaginationModel,
    filterItems: FilterCheckBox[]
}
