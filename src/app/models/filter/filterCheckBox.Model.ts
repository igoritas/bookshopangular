import { FilterColumnName } from '../enums';

export interface FilterCheckBox {
    columnName: FilterColumnName,
    value: number
}