export interface PaginationModel {
    pageNumber: number;
    pageSize: number;
}