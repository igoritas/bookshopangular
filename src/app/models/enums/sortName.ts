export enum SortName {
    Id = 0,
    Order = 1,
    CreationDate = 2,
    OrderPrice = 3,
    Price = 4
}