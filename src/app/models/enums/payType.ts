export enum PayType {
    None = 0,
    Unpaid = 1,
    Paid = 2,
}