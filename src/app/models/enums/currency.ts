export enum Currency {
    USD = 0,
    UAH = 1,
    EUR = 2,
    RUB = 3
}