export enum FilterColumnName {
	PrintingEditionType = 1,
	IsPaid = 2,
	IsActive = 3
}