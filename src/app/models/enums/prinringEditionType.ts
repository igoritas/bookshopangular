
export enum PrinringEditionType {
    Book = 0,
    Journal = 1,
    Newspaper = 2
}