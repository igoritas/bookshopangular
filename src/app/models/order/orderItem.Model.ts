import { PrintingEdition } from '../printingEdition/printingEdition.Model';

export interface OrderItems {
    id: string;
    book: PrintingEdition;
    count: number;
    price: number;
}
