import { BaseModel } from '../base/base.Model';
import { Currency, PayType } from '../enums';
import { OrderItems } from './orderItem.Model';
import { User } from '../user/user.Model';

export interface Order extends BaseModel {
    id: string;
    currency: Currency;
    totalPrice: number;
    orderItems: OrderItems[];
    user: User;
    transactionId: string;
    isPaid: PayType;
    date: Date;
}
