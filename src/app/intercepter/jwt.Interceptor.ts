import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { GlobalConstant } from 'src/app/sheared/global.constant';
import { CookiHelper } from 'src/app/helpers/cooki.helpers';
import { GlobalErrorHandler } from 'src/app/helpers/global-error-hendler';


@Injectable()
export class JwtInterceptor implements HttpInterceptor {
   constructor(
      private cookieHelper: CookiHelper,
      private globalErrorHandler: GlobalErrorHandler,
      private routService: Router,
      private authenticationService: AuthenticationService,
      private globalConstant: GlobalConstant) { }
   intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
      let currentUser = this.cookieHelper.get(this.globalConstant.accessToken);
      if (currentUser) {
         request = request.clone({
            setHeaders: {
               Authorization: `Bearer ${currentUser}`
            }
         });
      }
      request = request.clone({ headers: request.headers.set('Content-Type', 'application/json') });
      return next.handle(request).pipe(

         map((event: HttpEvent<any>) => {
            if (event instanceof HttpResponse) {
               console.log('event--->>>', event);
            }
            return event;
         }),
         catchError((error: HttpErrorResponse) => {
            let data = {};
            data = {
               reason: error.error.reason,
               status: error.status
            };
            if (error.status == this.globalConstant.errorAccess && this.cookieHelper.get(this.globalConstant.refreshToken)) {
               this.refreshToken(currentUser);
               return;
            }
            if (error.status == this.globalConstant.errorAccess) {
               this.routService.navigate(['/login']);
            }
            this.globalErrorHandler.handleError(error);
            return throwError(error);
         }));
   }
   private refreshToken(currentUser: string) {
      this.authenticationService.refreshToken(currentUser, this.cookieHelper.get(this.globalConstant.refreshToken)).subscribe(
         data => {
            this.routService.navigate(['/home']);
         });
   }
}